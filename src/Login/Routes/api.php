<?php

Route::namespace('Login\\Http\\Controllers')->group(function() {
  Route::post('login', 'LoginController@login');
  Route::middleware('auth:api')->group(function() {
    Route::get('logout', 'LoginController@logout');
  });
});