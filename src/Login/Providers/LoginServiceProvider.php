<?php

namespace Login\Providers;

use Illuminate\Support\ServiceProvider;

class LoginServiceProvider extends ServiceProvider
{
  public function boot()
  {
    $this->loadRoutesFrom(dirname(__DIR__, 1).'/Routes/api.php');
  }

  public function register()
  {

  }

}